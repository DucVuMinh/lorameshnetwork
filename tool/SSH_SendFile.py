import os
import paramiko
import time
print("Hello. Starting save log file gateway")
ssh = paramiko.SSHClient()
ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy)

# Tao Mang IP cua Gateway can ket noi. Gateway k ket noi dk thi them ky tu '#' de comment 
gateway = [\
'192.84.101.174', \
'192.84.102.214', \
'192.84.102.236', \
'192.84.103.226', \
'192.84.40.102', \
'192.84.40.146', \
'192.84.40.123', \
'192.84.91.100', \
'192.84.91.101', \
'192.84.91.102', \
'192.84.91.103', \
'192.84.91.104', \
'192.84.91.105', \
'192.84.91.111', \
'192.84.91.114', \
'192.84.91.143', \
'192.84.91.211', \
'192.84.91.215', \
'192.84.91.219', \
'192.84.91.232', \
'192.84.91.233', \
]

# Đường Link file muốn lấy của gateway 
path_server_1 = '/usr/share/iot-relay/main.py'
path_server_2 = '/usr/share/iot-relay/meikomqtt.py'
path_server_3 = '/usr/share/iot-relay/udpTransfar.py'

path_local_1 = 'D:\\Source_IoT\\IoT-GW-SensTranfer\\uploadcode_to_raspberry\\iot-relay\\main.py'
path_local_2 = 'D:\\Source_IoT\\IoT-GW-SensTranfer\\uploadcode_to_raspberry\\iot-relay\\meikomqtt.py'
path_local_3 = 'D:\\Source_IoT\\IoT-GW-SensTranfer\\uploadcode_to_raspberry\\iot-relay\\udpTransfar.py'
ip_len = len(gateway)
# Vòng lặp For chạy toàn bộ IP gateway có trong mảng đã khai báo từ trước
for x in range(ip_len):
    print("Connecting with gateway IP : %s" % (gateway[x]))
    ssh.connect(hostname=gateway[x],username='pi', password='raspberry',port=22)
    time.sleep(2)
    print("Permission .......CMD............")
    ssh.exec_command('cd /usr/share/iot-relay/')
    ssh.exec_command('sudo chmod 766 meikomqtt.py')
    sftp_client=ssh.open_sftp()
    sftp_client.put(path_local_1,path_server_1)
    sftp_client.put(path_local_2,path_server_2)
    sftp_client.put(path_local_3,path_server_3)
    sftp_client.close()
    ssh.exec_command('sudo reboot')
    ssh.close()
# Kết thúc
