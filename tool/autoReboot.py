import os
import time
import paramiko

h = 0
gateway= [\
'192.84.91.101', \
'192.84.103.223', \
'192.84.91.111', \
'192.84.91.215', \
'192.84.91.143', \
'192.84.91.219', \
'192.84.91.104', \
'192.84.102.236', \
'192.84.91.102', \
'192.84.91.103', \
'192.84.91.211', \
'192.84.91.131', \
'192.84.91.100', \
'192.84.101.174', \
'192.84.40.102', \
]
ssh = paramiko.SSHClient()
ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy)

while 1:
    os.system('cls')
    print("Hello. Starting Auto Reboot gateway")
    ip_len = len(gateway)
    # Vòng lặp For chạy toàn bộ IP gateway có trong mảng đã khai báo từ trước
    for x in range(ip_len):
        print("Connecting with gateway IP : %s" % (gateway[x]))
        ssh.connect(hostname=gateway[x],username='pi', password='raspberry',port=22)
        stdin, stdout, stderr = ssh.exec_command('sudo reboot')
        out = stdout.read().decode().strip()
        error = stderr.read().decode().strip()
        if out:
            print(out)
        if error:
            raise Exception('There was an error pulling the runtime: {}'.format(error))
        ssh.close()
        time.sleep(5)
    print ('Watting ...')
    time.sleep(3600*12)