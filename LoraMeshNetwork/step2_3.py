import pandas as pd 
import numpy as np 
import glob
import step1


def step2_3(para):
	#uids la tat ca uuid ma ng dung cung cap
	table1, data, statusIP, rasp_ip = step1.get_log(para)
	#uids = pd.unique(table1['UUID_Reciver'])
	#data = pd.read_excel('dataPrepare.xlsx')
	uids = data['UUID'].values
	table2 = []
	Status_UUID = []
	#UUID theo tung ip 
	UUID = table1['UUID_Reciver'].values
	RSSI = table1['RSSI'].values
	IP = table1['GW_Name'].values
	HITS = table1['Hits'].values
	Status = table1['Status'].values
	#UUID of GW's manage and index of them
	UUID_union = pd.unique(table1['UUID_Reciver'])
	ind_UUID = np.zeros(len(UUID_union))

	for uid in uids:
		#return index UUID ma co UUID == uid
		find = np.array(np.where(UUID == uid))
		find = np.reshape(find,find.size)
		#return index of UUID_union
		

		if find.size > 0:
			#set UUID_union
			findInUUID_union = np.array(np.where(UUID_union == uid))
			findInUUID_union = np.reshape(findInUUID_union,findInUUID_union.size)
			ind_UUID[findInUUID_union[0]] = 1


			Ind = find[0]
			Hits_val = int(HITS[Ind])
			for i in range(1,len(find)):
				index = find[i]
				if int(HITS[index]) >Hits_val:
					Hits_val = int(HITS[index])
					Ind = index
			#print(find, find.shape)
			#print(IP[Ind], UUID[Ind], RSSI[Ind], Status[Ind],'\n\n')
			table2.append([IP[Ind], UUID[Ind], RSSI[Ind], HITS[Ind], Status[Ind]])
			Status_UUID.append('OK')
		else:
			Status_UUID.append('UUIID_NG')

	dftb2 = pd.DataFrame(table2,
           columns=['GW_Name', 'UUID_Reciver', 'RSSI', 'HITS', 'Status'])
	#print('table2----------------------------------------------')
	#overite file dataPrepare
	#print(statusIP)

	table_data = {'IP': rasp_ip,'UUID':data['UUID'].values, 'StatusUUID':Status_UUID, 'StatusIP' : statusIP}
	dataPrepare =  pd.DataFrame(table_data,
                   columns=['IP', 'UUID', 'StatusUUID', 'StatusIP'])
	writer = pd.ExcelWriter('dataPrepare.xlsx', engine='xlsxwriter')
	dataPrepare.to_excel(writer, sheet_name='Sheet1')
	writer.save()

	#wirte vao` file chua UUID
	dtUUID = []
	for i in range(len(UUID_union)):
		if ind_UUID[i] == 0:
			dtUUID.append(UUID_union[i])
	#print(UUID_union)
	#print('_______________________')
	#print(dtUUID)
	data_UUID = {'UUID': dtUUID}
	df_UUID = pd.DataFrame(data_UUID,
                   columns=['UUID'])
	writer2 = pd.ExcelWriter('resultUUID.xlsx', engine='xlsxwriter')
	df_UUID.to_excel(writer2, sheet_name='Sheet1')
	writer2.save()
	print('STEP2---------------------------------')
	return dftb2