import glob

import numpy as np
import pandas as pd
import paramiko
import step2_3


def push_to_server(filetransfer, ip):
    print(ip + "push")

    localfilepath = filetransfer
    remotefilepath = "/etc/ddirm/ddirm.accept"
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(ip, username="pi", password="raspberry", port=22)
    print("File ddirm.accept........PUT................")
    ftp_client = ssh.open_sftp()
    ftp_client.put(localfilepath, remotefilepath)
    ftp_client.close()
    print("Reboot .......CMD............")
    stdin, stdout, stderr = ssh.exec_command("sudo reboot")
    out = stdout.read().decode().strip()
    error = stderr.read().decode().strip()
    if out:
        print(out)
    if error:
        raise Exception("There was an error pulling the runtime: {}".format(error))
    ssh.close()


def step4(para, excelfile):
    table2 = step2_3.step2_3(para, excelfile)
    # save ddirm.accept
    GW_Name = table2["GW_Name"].values
    # print(GW_Name)
    UUID = table2["UUID_Reciver"].values
    # lay gw khong trung nhau
    ip_union = pd.unique(table2["GW_Name"])

    # so sanh xem co thay doi khong va save ddirm.accept
    data = pd.read_excel("result.xlsx")
    GW_Name_prev = data["GW_Name"].values
    UUID_prev = data["UUID_Reciver"].values

    print("prev")
    print(data)
    print("curr")
    print(table2)
    for ip in ip_union:
        # print(ip)
        # tim ip trong danh sach truoc đó, ham nay` tra ve index
        find_in_prev = np.array(np.where(GW_Name_prev == ip))
        find_in_prev = np.reshape(find_in_prev, find_in_prev.size)
        print(find_in_prev)
        # tim ip trong danh sach hien tai, ham nay` tra ve index
        find_in_curr = np.array(np.where(GW_Name == ip))
        find_in_curr = np.reshape(find_in_curr, find_in_curr.size)
        print(find_in_curr)
        # kiem tra xem co ghi de` hay k, set la khong
        check = 0
        if (find_in_prev.size > 0) and (find_in_prev.size == find_in_curr.size):
            # cac uuid cua gw
            UUID_list = []
            for ip_pre in find_in_prev:
                UUID_list.append(UUID_prev[ip_pre])
            UUID_list = np.array(UUID_list)
            for ip_curr in find_in_curr:
                if (np.array(np.where(UUID_list == UUID[ip_curr]))).size == 0:
                    check = 1
                    break
        else:
            check = 1
        # check =1
        if check == 1:
            path = "accept/" + str(ip) + "_ddirm.accept"
            content = "# accept = UUID\n# type 2 - type 3\n"
            # print(find_in_curr)
            for ip_curr in find_in_curr:
                content += "accept = " + str(UUID[ip_curr]) + "\n"
            f = open(path, "w")
            f.write(content)
            f.close()
            # push_to_server(path,ip)
    writer = pd.ExcelWriter("result.xlsx", engine="xlsxwriter")
    table2.to_excel(writer, sheet_name="Sheet1")
    writer.close()
