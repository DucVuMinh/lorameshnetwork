import pandas as pd 
import numpy as np 
import paramiko
import glob
from datetime import datetime
import sys
import os.path

# save log file to laptop and create table
def get_log(excelfile):
	localfilepath = "fileLog/"
	remotefilepath = r"/var/ddirm/log/ddi_relay.log"
	table1 = []
	data = pd.read_excel(excelfile)
	rasp_ip = data['IP'].values
	statusIP = rasp_ip.copy()
	ip_len = len(rasp_ip)
	print ("SSH to Get Log from Gateway is Starting----", remotefilepath)
	for i in range(ip_len):
		ip = str(rasp_ip[i])
		ssh = paramiko.SSHClient()
		# Auto add host to known hosts
		try:
			ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
			ssh.connect(ip , username="pi", password="raspberry")
			print (i," ------------------",ip,)
			ftp_client=ssh.open_sftp()
			#set file name at laptop
			filelocalName = ip+'_'+ 'ddi_relay.log'
			local = localfilepath + filelocalName
			ftp_client.get(remotefilepath,local)
			ftp_client.close()
			#find UUID in GW
			statusIP[i] = 'Success'
		except Exception:
			#print(error)
			statusIP[i] = 'Fail'
		if(ip == 'nan'):
			break
#
if __name__ == "__main__":
    excelfile = input ("Enter file dataIPlist:")
    print("The file is: " + excelfile)
    print("Starting LoraMeshNetwork")
    get_log(excelfile)