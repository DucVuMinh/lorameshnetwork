import pandas as pd 
import numpy as np 
import paramiko
import glob
from datetime import datetime

def findUUID(localfilepath):
	filelog = open(localfilepath, 'r', encoding = 'cp850')
	lines = filelog.read().splitlines()
	list_line = []
	found = np.array([['UUID', 'RIIS', 'hits']])
	#duet tung dong trong 1 log file
	from datetime import timedelta
	d = timedelta(minutes = 360)
	date_curr = lines[len(lines)-1].split(' ')[0]
	time_curr = lines[len(lines)-1].split(' ')[1]
	datetime_curr = datetime.strptime(date_curr + time_curr, "%Y-%m-%d%H:%M:%S,%f")
	for line in lines[::-1]:
		if "COM:b'D:" in line:
			date = line.split(' ')[0]
			time = line.split(' ')[1]
			dateTime = datetime.strptime(date + time, "%Y-%m-%d%H:%M:%S,%f")
			#print(datetime_curr - dateTime)
			if datetime_curr - dateTime < d:
				uid = (((line.split(':'))[-1]).split(','))[0]
				index = np.array(np.where(found == uid))
				index = np.reshape(index,index.size)
				RSSI_val = (line.split(','))[-3]
				RSSI_val = -(65535 - int(RSSI_val,16))
				list_line.append(uid)
				if(index.size == 0):
					found = np.append(found, [[uid, RSSI_val, 0]], axis=0)
				else:
					found[index[0], 1] = RSSI_val
			else:
				break
	list_line_len = len(found)
	for xxx in range(1, list_line_len):
		counter = list_line.count(found[xxx,0])
		found[xxx, 2] = counter
	return(found)




# save log file to laptop and create table
def get_log(para,excelfile):
	localfilepath = "../fileLog/"
	remotefilepath = r"/var/ddirm/log/ddi_relay.log"
	table1 = []
	data = pd.read_excel(excelfile)
	rasp_ip = data['IP'].values
	statusIP = rasp_ip.copy()
	ip_len = len(rasp_ip)
	print ("SSH to Get Log from Gateway is Starting----", remotefilepath)
	for i in range(ip_len):
		ip = str(rasp_ip[i])

		#ssh = paramiko.SSHClient()
		# Auto add host to known hosts
		try:
			#ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())

			#ssh.connect(ip , username="pi", password="raspberry")

			print (i," ------------------",ip,)
			#ftp_client=ssh.open_sftp()

			#set file name at laptop
			filelocalName = ip+'_'+ 'ddi_relay.log'
			local = localfilepath + filelocalName
			#ftp_client.get(remotefilepath,local)
			#ftp_client.close()
			#find UUID in GW

			found = findUUID(local)
			#print (ip)
			if found.size >1:
				for j in range(1,len(found)):
					if (int(found[j,1]) > -para):
						status = 'OK'
					else:
						status = 'NG'
					table1.append([ip,found[j,0],found[j,1],found[j,2],status])
			statusIP[i] = 'Success'

		except Exception:
			#print(error)
			statusIP[i] = 'Fail'
		
		if(ip == 'nan'):
			break
	statusIP[i:len(statusIP)-1] = ' '

	#dataframe
	dftb1 = pd.DataFrame(table1,
                   columns=['GW_Name', 'UUID_Reciver', 'RSSI', 'Hits', 'Status'])
	#ssh.close()
	print ('STEP1------------------')
	writer = pd.ExcelWriter('table1.xlsx', engine='xlsxwriter')
	dftb1.to_excel(writer, sheet_name='Sheet1')
	writer.save()
	#return dftb1, uids

	return dftb1, data, statusIP, rasp_ip